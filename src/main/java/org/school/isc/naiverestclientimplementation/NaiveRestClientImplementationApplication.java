package org.school.isc.naiverestclientimplementation;

import org.school.isc.api.SimplePersonApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class NaiveRestClientImplementationApplication {

    public static void main(String[] args) {
        SpringApplication.run(NaiveRestClientImplementationApplication.class, args);

        final SimplePersonApi simplePersonApi = new SimplePersonApi();
        System.out.println(simplePersonApi.getSimplePerson("Rafael", "Yakupov"));
        //SOLVED questions - how to retrieve data in pojo, not String
        //SOLVED what happen if smth change in service rest api

    }

}
